#!/usr/bin/env python
import sys
import re
import os
import json
import webbrowser

from gmail_helper import GmailHelper

class EmailGenerator(object):
    variable_regex = '\%\((?P<name>\w+)\)s'

    def __init__(self, template_file, data_file):
        self.template_variables = {
            'FROM': None,
            'SUBJECT': None,
            'CONTENT': None,
            'COMMENT_ENTRY': None,
            'COMMENT_SECTION': None,
        }
        self.data = {}

        self.config = {}
        self._load_user_config()

        self.template_file = template_file
        self.data_file = data_file

        self._init_data()
        template_file = self.template_file.replace('.py', '').split('/')[-1]
        data_file_path = os.path.join('data', template_file)
        if not self.data_file and os.path.exists(data_file_path):
            self.data_file = data_file_path

        if self.data_file:
            self._parse_data_file()
        else:
            self._save_default_data()

        print "Your data will be save at '%s'" % self.data_file

    def _load_user_config(self):
        try:
            f = open('.email_config', 'r')
            data = json.loads(f.read())
            self.config['username'] = data['username']
            self.config['password'] = data['password']
            f.close()
        except:
            self.config['username'] = 'thangmeo2020@gmail.com'
            self.config['password'] = '@123456@'
            self._save_user_info()

    def _save_user_info(self):
        try:
            f = open('.email_config', 'w')
            f.write(json.dumps(self.config))
            f.close()
        except:
            pass

    def _init_data(self):
        template_file = self.template_file.replace('.py', '')
        template = __import__(template_file.replace('/', '.'), globals(),
                locals(), self.template_variables.keys(), -1)

        try:
            self.template_variables['FROM'] = template.FROM
            self.template_variables['SUBJECT'] = template.SUBJECT
            self.template_variables['CONTENT'] = template.CONTENT
            self.template_variables['COMMENT_ENTRY'] = template.COMMENT_ENTRY
            self.template_variables['COMMENT_SECTION'] = template.COMMENT_SECTION
        except:
            pass

        for name, content in self.template_variables.items():
            if content:
                variables = re.findall(self.variable_regex, content)
                self.data.update({name: {k:k for k in variables}})

    def _save_default_data(self):
        template_file = self.template_file.replace('.py', '').split('/')[-1]
        data = self.generate_data()
        data_file_path = os.path.join('data', template_file)
        f = open(data_file_path, 'w')
        f.write(data)
        f.close()


    def _parse_data_file(self):
        try:
            f = open(self.data_file)
            data = json.loads(f.read())
            self.data.update(data)
        except Exception, e:
            print e.message

    def _create_html_file(self, filepath):
        f = open(filepath, 'w')
        if self.template_variables['COMMENT_SECTION']:
            comment_entry = self.template_variables['COMMENT_ENTRY'] % self.data['COMMENT_ENTRY']

            try:
                num_of_entry = int(self.data['COMMENT_SECTION']['comment_list'])
            except:
                num_of_entry= 1

            self.data['COMMENT_SECTION']['comment_list'] = ''
            for i in xrange(num_of_entry):
                self.data['COMMENT_SECTION']['comment_list'] += comment_entry

            comment_section = self.template_variables['COMMENT_SECTION'] % self.data['COMMENT_SECTION']
            self.data['CONTENT']["comment_section"] = comment_section
            content = self.template_variables['CONTENT'] % self.data['CONTENT']
        else:
            content = self.template_variables['CONTENT'] % self.data['CONTENT']
        f.write(content)
        f.close()

    def openbrowser(self, filepath):
        self._create_html_file(filepath)
        url = 'file://localhost/%s' % filepath
        webbrowser.open(url)

    def generate_data(self):
        return json.dumps(self.data, indent=4)

    def send_email(self, to_address):
        client = GmailHelper(self.config['username'], self.config['password'])
        from_address = self.template_variables['FROM'] % self.data['FROM']
        subject = self.template_variables['SUBJECT'] % self.data['SUBJECT']

        try:
            comment_entry = self.template_variables['COMMENT_ENTRY'] % self.data['COMMENT_ENTRY']
        except:
            pass

        content = self.template_variables['CONTENT'] % self.data['CONTENT']
        client.mail(from_address, to_address, subject, content)

USAGE = """
    email_tool.py [cmd] <template_file_path> <data_file_path> <email_address>
    Where:
        * cmd - web: create html from template file and open web browser to test
              - email: create email and send to user
              - gen_data: generate data for template file and print to screen
        * web command:
            email_tool.py web <template_file_path> <data_file_path>
          Where
            template_file_path: path to template file
            data_file_path (optional): path to data file. If this parameter is not
              provided, email_tool will get default `data_file_path` from
              data/<template_file_name>
        * If you not sure about data file, use `gen_data` command to generate it
            email_tool.py gen_data <template_file_path>
          Where template_file_path: path to template file
        * If you want to send email by a template, use command `email`
            email_tool.py email <template_file_path> <data_file_path> <email_address>
"""
def main():
    length = len(sys.argv)
    if length < 2:
        print USAGE
    else:
        cmd = sys.argv[1]
        if cmd in ("web", "email", "gen_data"):
            template_file = sys.argv[2] if length > 2 else None
            if cmd == "web":
                data_file = sys.argv[3] if length > 3 else None
            elif cmd == "email":
                data_file = sys.argv[3] if length > 4 else None
            else:
                data_file = None

            if not template_file:
                print USAGE
            else:
                generator = EmailGenerator(template_file, data_file)
                if cmd == 'web':
                    generator.openbrowser(os.path.join(os.getcwd(), 'tmp', 'test.html'))
                elif cmd == 'gen_data':
                    data = generator.generate_data()
                    print "=============="
                    print data
                elif cmd == 'email':
                    try:
                        to_address = sys.argv[4] if length > 4 else sys.argv[3]
                    except:
                        print USAGE
                        print "Please provide to_address for email"
                        return
                    generator.send_email(to_address)
        else:
            print USAGE

if __name__ == "__main__":
    main()
