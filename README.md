# **NOTE**
For ``event_commented.py`` template file, data file will have format

```
{
    "FROM": {},
    "COMMENT_ENTRY": {
        "comment": "comment",
        "avatar_link": "avatar_link",
        "firstname": "firstname"
    },
    "CONTENT": {
        "preference_link": "preference_link",
        "event_title": "event_title",
        "account_firstname": "account_firstname",
        "comment_section": "comment_section",
        "actor": "actor",
        "action": "action"
    },
    "COMMENT_SECTION": {
        "event_title": "event_title",
        "event_link": "event_link",
        "comment_list": "comment_list"
    },
    "SUBJECT": {
        "event_title": "event_title",
        "action": "action",
        "actor": "actor"
    }
}
```

You should change "COMMENT_SECTION"."comment_list" to number of comment you want
to display.

E.g:
```
{
    "FROM": {},
    "COMMENT_ENTRY": {
        "comment": "comment",
        "avatar_link": "avatar_link",
        "firstname": "firstname"
    },
    "CONTENT": {
        "preference_link": "preference_link",
        "event_title": "event_title",
        "account_firstname": "account_firstname",
        "comment_section": "comment_section",
        "actor": "actor",
        "action": "action"
    },
    "COMMENT_SECTION": {
        "event_title": "event_title",
        "event_link": "event_link",
        "comment_list": 5
    },
    "SUBJECT": {
        "event_title": "event_title",
        "action": "action",
        "actor": "actor"
    }
}
```

# **I. Email tool for product owner**

``email_tool`` provide 3 commands ``gen_data`` to auto generate data from template file,
    ``web`` data to open template file in web browser, and ``email`` to send email template to specified user

## **1. gen_data**
Generate data for template file. This command parses template file and provides default value for it

**Mandatory parameter**

* ``template_path``: path to template file. Template file is python file provide template for email address
  Each template file includes at least 3 parts: ``FROM``, ``SUBJECT``, ``CONTENT``.
  Each part includes some python variables.
  To generate HTML, we need to provide variables for template file

Example of temlate file ``account_confirm_additional_email.py``


```
FROM = "Klamr"
SUBJECT = "Link this email address to your Klamr account"
CONTENT = """
<table border="0" cellpadding="0" cellspacing="3px" style="font-family:arial;width:100%%;">
    <!-- begin event -->
    <tr>
        <td height="78px" style="background-color:#e6e6e6;">
            <table border="0" cellpadding="0" cellspacing="0" style="width:100%%;">
                <tr>
                    <td>
                        <h1 style="font-size:18px;color:#fff;background-color:#28a9e0;margin:0;padding-top:10px;padding-bottom:10px;padding-left:14px;">Add a new email to your Klamr account</h1>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align:top; overflow:hidden;padding:10px 14px;color:#4d4d4d;font-size:14px;line-height:17px;">
                        <p>
                        Hi %(first_name)s, </p>
                        <p>We've detected that you're using two email addresses to receive Klamr invitations!</p>
                        <p>To simplify things you can add this email address (%(new_email_address)s) to your existing Klamr account by following the link below.  Any past or new Klamr invitations you receive at this email address will appear under your existing Klamr account.</p>
                        <p>%(link)s</p>
                        <p>If you do not wish to add this email address to your Klamr account simply ignore this email.</p>
                        <p>Thanks,<br>
                        The Klamr Team</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!-- end event -->
    <tr>
       <td height="62px" style="background-color:#fff; width:158px; height:62px;">
           <table border="0" cellpadding="0" cellspacing="0">
               <tr>
                   <td style="vertical-align:top;">
                       <img style="float:left;" src="http://ktm.to/klamr/email_assets/EmailKlamrLogo.png" width="158px" height="62px" alt="Klamr">
                   </td>
                   <td style="font-size:11px;color:#a2a2a2;">
                       <p style="padding:5px 0;margin:0;">This email was sent by <span style="color:#28a9e0;">Klamr.</span></p>
                       <p style="padding:5px 0;margin:0;">To change how we contact you, please follow this link: <a href="%(preference_link)s" style="color:#28a9e0; text-decoration:none;">%(preference_link)s</a></p>
                   </td>
               </tr>
               <tr>
                   <td colspan="2" style="font-size:11px;color:#a2a2a2;">
                       <p style="text-align:left; padding:5px 0;margin:0;">If you would like to unsubcribe and stop receiving these emails <a href="[unsubscribe]" style="color:#28a9e0; text-decoration:none;">click here</a></p>
                   </td>
               </tr>
           </table>
       </td>
    </tr>
</table>
"""
```

In this template file, we need provides 4 parameters for ``CONTENT`` part:

* firs_name
* link
* preference_link
* new_email_address

Python variables was provided by ``data_path`` file. In our example, ``data_path`` file should have format
```
{
    "CONTENT": {
        "first_name": "first_name",
        "link": "link",
        "preference_link": "preference_link",
        "new_email_address": "new_email_address"
    },
    "FROM": {},
    "SUBJECT": {}
}
```

**Example usage**
```
$> ./email_tool.py gen_data template/account_confirm_additional_email.py
Your data will be save at 'data/account_confirm_additional_email'
==============
{
    "CONTENT": {
        "first_name": "first_name",
        "link": "link",
        "preference_link": "preference_link",
        "new_email_address": "new_email_address"
    },
    "FROM": {},
    "SUBJECT": {}
}
```

## **2. web**
This command generate HTML file from template file and data file, and open web browser to view it

**Madatory paramters**

* ``template_file_path``: Path to template file

**Optional parameters**

* ``data_file_path``: Path to data file. If this parameter was not provided, ``email_tool`` will get data from default file at ``data/<template_file_name>``. If this default file was not exist, default value will be taken to provide for ``template_data_file_path``

Default value of a variable is this variable it self.

E.g:
Default value for ``first_name`` variable is "first_name"

**Example usage**

```
# firstly, gen_data
$> ./email_tool.py gen_data templates/account_confirm_additional_email.py
Your data will be save at 'data/account_confirm_additional_email'

# then edit the data file by your editor

# finally, run web command
$> ./email_tool.py web templates/account_confirm_additional_email.py
```

## **3. email**
This command generate HTML file and send it to a specified email.

This command use default gmail account to send email. Default email was set in .email_config file.

This file contains data with format:

```
{
    "username": "thangmeo2020@gmail.com",
    "password": "@123456@"
}
```

Where
``username``: email address was used
``password``: password of that email address

You can modify this file to use your own email


**Madatory paramters**

* ``template_file_path``: Path to template file

* ``email_address``: Email address to be sent the email

**Optional parameters**

* ``data_file_path``: Path to data file. If this parameter was not provided, ``email_tool`` will get data from default
file at ``data/<template_file_name>``. If this default file was not exist, default value will be taken to provide for ``template_data_file_path``


**Example usage**

```
# firstly, gen_data
$> ./email_tool.py gen_data templates/account_confirm_additional_email.py
Your data will be save at 'data/account_confirm_additional_email'

# then edit the data file by your editor

# finally, run web command to send email to kien.nguyent@skunkworks.vn
$> ./email_tool.py email templates/account_confirm_additional_email.py kien.nguyent@skunkworks.vn
```

# **II. Set up tool**
```
$> cd <path_you_want_to_store>
# clone server api core
$> git clone git@bitbucket.org:skunkworksvn/server_api_core.git

# clone email tool
$> git clone git@bitbucket.org:kiennguyent/email_tool.git
$> cd email_tool
# create data folder to store default data file
$> mkdir data
```
